FROM microsoft/dotnet:2.2-sdk as build
WORKDIR /api
COPY . ./
RUN dotnet restore &&  dotnet publish -c Release -o Project


FROM microsoft/dotnet:2.2-aspnetcore-runtime
ENV TZ=America/Sao_Paulo ASPNETCORE_ENVIRONMENT=staging
COPY --from=build /api/Project ./app
WORKDIR /app
RUN echo 'America/Sao_Paulo' > /etc/timezone
ENTRYPOINT ["dotnet", "backend.dll"]